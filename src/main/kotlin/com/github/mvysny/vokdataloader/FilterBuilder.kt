package com.github.mvysny.vokdataloader

import java.io.Serializable
import kotlin.reflect.KProperty1

/**
 * Creates a filter programmatically: `buildFilter { Person::age lt 25 }`
 */
inline fun <reified T : Any> buildFilter(block: FilterBuilder<T>.() -> Filter<T>): Filter<T> = block(FilterBuilder(T::class.java))

/**
 * Running block with this class as its receiver will allow you to write expressions like this:
 * `Person::age lt 25`. Does not support joins - just use the plain old SQL 92 where syntax for that ;)
 *
 * Containing these functions in this class will prevent polluting of the KProperty1 interface and also makes it type-safe.
 *
 * This looks like too much Kotlin syntax magic. Promise me to use this for simple Entities and/or programmatic where creation only ;)
 * @param clazz builds the query for this class.
 */
class FilterBuilder<T : Any>(val clazz: Class<T>) {
    /**
     * Creates an [EqFilter], requesting given property value to be equal to given [value].
     */
    infix fun <R : Serializable?> KProperty1<T, R>.eq(value: R): Filter<T> = EqFilter(name, value)

    /**
     * Creates an [OpFilter] with [CompareOperator.le], requesting given property value to be less-than or equal to given [value].
     */
    @Suppress("UNCHECKED_CAST")
    infix fun <R> KProperty1<T, R?>.le(value: R): Filter<T> =
            OpFilter(name, value as Comparable<Any>, CompareOperator.le)

    /**
     * Creates an [OpFilter] with [CompareOperator.lt], requesting given property value to be less-than given [value].
     */
    @Suppress("UNCHECKED_CAST")
    infix fun <R> KProperty1<T, R?>.lt(value: R): Filter<T> =
            OpFilter(name, value as Comparable<Any>, CompareOperator.lt)

    /**
     * Creates an [OpFilter] with [CompareOperator.ge], requesting given property value
     * to be greater-than or equal to given [value].
     */
    @Suppress("UNCHECKED_CAST")
    infix fun <R> KProperty1<T, R?>.ge(value: R): Filter<T> =
            OpFilter(name, value as Comparable<Any>, CompareOperator.ge)

    /**
     * Creates an [OpFilter] with [CompareOperator.gt], requesting given
     * property value to be greater-than given [value].
     */
    @Suppress("UNCHECKED_CAST")
    infix fun <R> KProperty1<T, R?>.gt(value: R): Filter<T> =
            OpFilter(name, value as Comparable<Any>, CompareOperator.gt)

    /**
     * Creates an [InFilter], requesting given property value to be one of the values
     * provided in the [value] collection.
     */
    @Suppress("UNCHECKED_CAST")
    infix fun <R : Collection<Comparable<*>>> KProperty1<T, R>.`in`(value: R): Filter<T> = InFilter(name, value)

    /**
     * A LIKE filter. It performs the 'starts-with' matching which tends to perform quite well on indexed columns. If you need a substring
     * matching, then you actually need to employ full text search
     * capabilities of your database. For example [PostgreSQL full-text search](https://www.postgresql.org/docs/9.5/static/textsearch.html).
     *
     * There is no point in supporting substring matching: it performs a full table scan when used, regardless of whether the column contains
     * the index or not. If you really wish for substring matching, you probably want a full-text search instead which is implemented using
     * a different keywords.
     * @param prefix the prefix, automatically appended with `%` when the SQL query is constructed. The 'starts-with' is matched
     * case-sensitive.
     */
    infix fun KProperty1<T, String?>.like(prefix: String): Filter<T> = LikeFilter(name, prefix)

    /**
     * An ILIKE filter, performs case-insensitive matching. It performs the 'starts-with' matching which tends to perform quite well on indexed columns. If you need a substring
     * matching, then you actually need to employ full text search
     * capabilities of your database. For example [PostgreSQL full-text search](https://www.postgresql.org/docs/9.5/static/textsearch.html).
     *
     * There is no point in supporting substring matching: it performs a full table scan when used, regardless of whether the column contains
     * the index or not. If you really wish for substring matching, you probably want a full-text search instead which is implemented using
     * a different keywords.
     * @param prefix the prefix, automatically appended with `%` when the SQL query is constructed. The 'starts-with' is matched
     * case-insensitive.
     */
    infix fun KProperty1<T, String?>.ilike(prefix: String): Filter<T> = ILikeFilter(name, prefix)

    /**
     * Matches only values contained in given range.
     * @param range the range
     */
    infix fun <R> KProperty1<T, R?>.between(range: ClosedRange<R>): Filter<T> where R : Number, R : Comparable<R> =
            this.ge(range.start as Number) and this.le(range.endInclusive as Number)

    /**
     * Matches only when the property is null. Uses [IsNullFilter].
     */
    val KProperty1<T, *>.isNull: Filter<T> get() = IsNullFilter(name)

    /**
     * Matches only when the property is not null. Uses [IsNotNullFilter].
     */
    val KProperty1<T, *>.isNotNull: Filter<T> get() = IsNotNullFilter(name)

    /**
     * Matches only when the property is true. Uses [EqFilter] with the value of `true`.
     */
    val KProperty1<T, Boolean?>.isTrue: Filter<T> get() = EqFilter(name, true)

    /**
     * Matches only when the property is false. Uses [EqFilter] with the value of `false`.
     */
    val KProperty1<T, Boolean?>.isFalse: Filter<T> get() = EqFilter(name, false)

    /**
     * Allows for a native query: `"age < :age_p"("age_p" to 60)`
     */
    operator fun String.invoke(vararg params: Pair<String, Any?>): Filter<T> = NativeSqlFilter<T>(this, mapOf(*params))

    /**
     * A LIKE filter which performs the case-sensitive 'substring' matching. Usually only used for in-memory
     * filtering since it performs quite poorly in the database.
     *
     * *WARNING:* The database performance is very poor, even on indexed columns - the database effectively performs full
     * table scan. Instead you should use the [FullTextFilter] and the full text search
     * capabilities of your database, combined with
     * the [NativeSqlFilter]. For example see [PostgreSQL full-text search](https://www.postgresql.org/docs/9.5/static/textsearch.html).
     */
    infix fun KProperty1<T, String?>.contains(prefix: String): Filter<T> = SubstringFilter(name, prefix)

    /**
     * A LIKE filter which performs the case-insensitive 'substring' matching. Usually only used for in-memory
     * filtering since it performs quite poorly in the database.
     *
     * *WARNING:* The performance is very poor, even on indexed columns - the database effectively performs full
     * table scan. Instead you should use the [FullTextFilter].
     */
    infix fun KProperty1<T, String?>.icontains(prefix: String): Filter<T> = ISubstringFilter(name, prefix)
}

@Deprecated("Use FilterBuilder", replaceWith = ReplaceWith("FilterBuilder<T>"))
typealias SqlWhereBuilder<T> = FilterBuilder<T>
