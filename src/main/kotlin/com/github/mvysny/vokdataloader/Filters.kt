package com.github.mvysny.vokdataloader

import java.io.Serializable

/**
 * A generic filter which filters items of type [T]. Implementors must precisely declare in kdoc how exactly the items are filtered.
 *
 * Implementor detail: [Any.equals]/[Any.hashCode]/[Any.toString] must be implemented properly, so that the filters can be
 * placed in a set. As a bare minimum, the filter type, the property name and the value which we compare against must be
 * taken into consideration.
 *
 * The [test] method is provided only as a convenience. There may be filters which
 * can not implement this kind of method
 * (e.g. REST-server-specific filters, or filters representing a SQL where clause).
 * Such filters should document this
 * behavior and may throw [UnsupportedOperationException] in the [test] method.
 * @param T the bean type upon which we will perform the filtering.
 */
interface Filter<T : Any> : Serializable {
    /**
     * Evaluates this predicate on [t]. Returns `true` if the input argument
     * matches the predicate, otherwise `false`
     */
    fun test(t: T): Boolean

    infix fun and(other: Filter<in T>): Filter<T> = AndFilter(setOf<Filter<in T>>(this, other))
    infix fun or(other: Filter<in T>): Filter<T> = OrFilter(setOf<Filter<in T>>(this, other))

    /**
     * Returns a filter that represents the logical negation of this filter.
     */
    operator fun not(): Filter<T> = NotFilter(this)
}

/**
 * Filters beans by comparing a property value with given [propertyName]
 * to some expected [value]. Check out implementors for further details.
 *
 * The filter's [test] method is able to retrieve values of [propertyName]
 * containing a [PropertyPath]. This is only useful for in-memory filtering:
 * other types of filters generally may not support [PropertyPath]s or may interpret
 * them differently. For example a SQL Data Provider will interpret `e.name`
 * as the `name` column of table `EMPLOYEE` aliased as `e` in the select statement,
 * while [test] method will be unable to find the `e` property and will fail.
 *
 * @property propertyName a valid data loader property name.
 * See [DataLoaderPropertyName] for a precise definition.
 * @property value optional value to compare against.
 */
abstract class BeanFilter<T : Any> : Filter<T> {
    abstract val propertyName: DataLoaderPropertyName
    abstract val value: Any?

    @Transient
    private var getter: PropertyPath<T>? = null

    /**
     * Gets the value of the [propertyName] for given [bean].
     * Very quick even though it uses reflection under the hood.
     *
     * Also able to retrieve a value of a property path, see [PropertyPath] for more details.
     */
    protected fun getValue(bean: T): Any? {
        var g: PropertyPath<T>? = getter
        if (g == null) {
            g = PropertyPath.of(bean.javaClass, propertyName)
            getter = g
        }
        return g.invoke(bean)
    }

    protected val formattedValue: String? get() = if (value != null) "'$value'" else null
}

/**
 * A filter which tests for value equality. Allows nulls.
 */
data class EqFilter<T : Any>(override val propertyName: DataLoaderPropertyName, override val value: Any?) : BeanFilter<T>() {
    override fun toString(): String = "$propertyName = $formattedValue"
    override fun test(t: T): Boolean = getValue(t) == value
}

enum class CompareOperator(val sql92Operator: String) {
    eq("=") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?) = comparator.compare(t, u) == 0
    },
    lt("<") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?) = comparator.compare(t, u) < 0
    },
    le("<=") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?) = comparator.compare(t, u) <= 0
    },
    gt(">") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?) = comparator.compare(t, u) > 0
    },
    ge(">=") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?) = comparator.compare(t, u) >= 0
    };

    /**
     * Compares two numbers (or comparable objects in general), returning true if
     * the values comply with the compare operator.
     */
    abstract fun test(t: Comparable<Any>?, u: Comparable<Any>?): Boolean

    companion object {
        /**
         * The default comparator used to compare values. Defaults to [DefaultFilterComparator]`.nullsFirst()`;
         * set to something else to support more comparisons (e.g. date-based comparisons).
         */
        var comparator: Comparator<Comparable<Any>> = DefaultFilterComparator.nullsFirst()
    }
}

/**
 * A filter which supports less than, less or equals than, etc. Filters out null values.
 */
data class OpFilter<T : Any>(override val propertyName: DataLoaderPropertyName, override val value: Comparable<*>, val operator: CompareOperator) : BeanFilter<T>() {
    override fun toString(): String = "$propertyName ${operator.sql92Operator} $formattedValue"

    @Suppress("UNCHECKED_CAST")
    override fun test(t: T): Boolean = operator.test(getValue(t) as Comparable<Any>?, value as Comparable<Any>?)
}

/**
 * Checks if the [value]-collection contains the property value.
 *
 * The in-memory matching is performed based on Collections.contains, other data providers may specify different
 * criteria for equality.
 * @param value The collection to check against, must not be empty.
 */
data class InFilter<T : Any>(
        override val propertyName: DataLoaderPropertyName,
        override val value: Collection<Comparable<*>>
) : BeanFilter<T>() {
    init {
        check(value.isNotEmpty()) { "value: cannot be empty" }
    }

    override fun test(t: T): Boolean = value.contains(getValue(t))
    override fun toString(): String =
            value.joinToString(", ", prefix = "$propertyName in (", postfix = ")") { "'$it'" }
}

/**
 * Checks that property value is null.
 */
data class IsNullFilter<T : Any>(override val propertyName: DataLoaderPropertyName) : BeanFilter<T>() {
    override val value: Any? = null
    override fun test(t: T): Boolean = getValue(t) == null
    override fun toString(): String = "$propertyName IS NULL"
}

/**
 * Checks that the property value is not null.
 *
 * *Implementation detail*: [value] returns null even though this filter matches non-null values.
 */
data class IsNotNullFilter<T : Any>(override val propertyName: DataLoaderPropertyName) : BeanFilter<T>() {
    /**
     * Do not rely on the value in this particular case; it's impossible to represent
     * an universal set with a finite sets provided by Kotlin.
     */
    override val value: Any? = null
    override fun test(t: T): Boolean = getValue(t) != null
    override fun toString(): String = "$propertyName IS NOT NULL"
}

/**
 * A LIKE filter. It performs the 'starts-with' matching which tends to perform quite well on indexed columns.
 *
 * See [SubstringFilter] for substring matching, but beware of its poor performance on the database.
 * @param startsWith the prefix, automatically appended with `%` when the SQL query is constructed. The 'starts-with' is matched
 * case-sensitive.
 */
class LikeFilter<T : Any>(override val propertyName: DataLoaderPropertyName, startsWith: String) : BeanFilter<T>() {
    val startsWith = startsWith.trim()
    override val value = "${this.startsWith}%"
    override fun toString() = "$propertyName LIKE $formattedValue"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as LikeFilter<*>
        if (propertyName != other.propertyName) return false
        if (value != other.value) return false
        return true
    }

    override fun hashCode(): Int {
        var result: Int = propertyName.hashCode()
        result = 31 * result + value.hashCode()
        return result
    }

    override fun test(t: T): Boolean {
        val v = getValue(t) as? String ?: return false
        return v.startsWith(startsWith)
    }
}

/**
 * An ILIKE filter. It performs the 'starts-with' matching which tends to perform quite well on indexed columns.
 *
 * See [ISubstringFilter] for substring matching, but beware of its poor performance on the database.
 * @param startsWith the prefix, automatically appended with `%` when the SQL query is constructed. The 'starts-with' is matched
 * case-insensitive.
 */
class ILikeFilter<T : Any>(override val propertyName: DataLoaderPropertyName, startsWith: String) : BeanFilter<T>() {
    val startsWith = startsWith.trim()
    override val value = "${this.startsWith}%"
    override fun toString() = "$propertyName ILIKE $formattedValue"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as ILikeFilter<*>
        if (propertyName != other.propertyName) return false
        if (value != other.value) return false
        return true
    }

    override fun hashCode(): Int {
        var result: Int = propertyName.hashCode()
        result = 31 * result + value.hashCode()
        return result
    }

    override fun test(t: T): Boolean {
        val v: String = getValue(t) as? String ?: return false
        return v.startsWith(startsWith, ignoreCase = true)
    }
}

/**
 * NOT filter: negates the result of [child]
 * @param child child filter
 */
class NotFilter<T : Any>(val child: Filter<in T>) : Filter<T> {
    override fun toString() = "not ($child)"
    override fun test(t: T): Boolean = !child.test(t)
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as NotFilter<*>
        if (child != other.child) return false
        return true
    }

    override fun hashCode(): Int = child.hashCode()
}

/**
 * AND filter: matches item only when all [children] matches.
 * @param children child filters, must not be empty.
 */
class AndFilter<T : Any>(children: Set<Filter<in T>>) : Filter<T> {
    init {
        check(children.isNotEmpty()) { "children: cannot be empty" }
    }

    val children: Set<Filter<in T>> = children.flatMap { if (it is AndFilter) it.children else listOf(it) }.toSet()
    override fun toString() = children.joinToString(" and ", "(", ")")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as AndFilter<*>
        if (children != other.children) return false
        return true
    }

    override fun hashCode() = children.hashCode()
    override fun test(t: T): Boolean = children.all { it.test(t) }
}

/**
 * OR filter: matches item only when at least one of [children] matches.
 * @param children child filters, must not be empty.
 */
class OrFilter<T : Any>(children: Set<Filter<in T>>) : Filter<T> {
    init {
        check(children.isNotEmpty()) { "children: cannot be empty" }
    }

    val children: Set<Filter<in T>> = children.flatMap { if (it is OrFilter) it.children else listOf(it) }.toSet()
    override fun toString() = children.joinToString(" or ", "(", ")")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as OrFilter<*>
        if (children != other.children) return false
        return true
    }

    override fun hashCode() = children.hashCode()
    override fun test(t: T): Boolean = children.any { it.test(t) }
}

/**
 * ANDs given set of filters and produces an [AndFilter].
 * @return ANDed filters; `null` when receiver is empty; if receiver contains exactly one filter, that filter is simply returned.
 */
fun <T : Any> Set<Filter<T>>.and(): Filter<T>? = when (size) {
    0 -> null
    1 -> first()
    else -> AndFilter(this)
}

/**
 * ORs given set of filters and produces an [OrFilter].
 * @return ORed filters; `null` when receiver is empty; if receiver contains exactly one filter, that filter is simply returned.
 */
fun <T : Any> Set<Filter<T>>.or(): Filter<T>? = when (size) {
    0 -> null
    1 -> first()
    else -> OrFilter(this)
}

/**
 * Just write any native SQL into [where], e.g. `age > 25 and name like :name`; don't forget to properly fill in the [params] map.
 *
 * Does not support in-memory filtering and will throw an exception.
 */
data class NativeSqlFilter<T : Any>(val where: String, val params: Map<String, Any?>) : Filter<T> {
    override fun test(t: T): Boolean = throw UnsupportedOperationException("Does not support in-memory filtering")
    override fun toString() = "$where$params"
}

/**
 * A LIKE filter which performs the case-sensitive 'substring' matching. Usually only used for in-memory
 * filtering since it performs quite poorly in the database.
 *
 * *WARNING:* The database performance is very poor, even on indexed columns - the database effectively performs full
 * table scan. Instead you should use the [FullTextFilter] and the full text search
 * capabilities of your database; alternatively use [NativeSqlFilter] to write a query suited
 * for the database of your choice. For example see [PostgreSQL full-text search](https://www.postgresql.org/docs/9.5/static/textsearch.html).
 * @param contains the substring, automatically prepended and appended with `%` when the SQL query is constructed. The 'substring' is matched
 * case-sensitive.
 */
class SubstringFilter<T : Any>(override val propertyName: DataLoaderPropertyName, contains: String) : BeanFilter<T>() {
    /**
     * Any probe must contain this string.
     */
    val contains: String = contains.trim()
    override val value = "%${this.contains}%"
    override fun toString() = "$propertyName LIKE $formattedValue"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as SubstringFilter<*>
        if (propertyName != other.propertyName) return false
        if (value != other.value) return false
        return true
    }

    override fun hashCode(): Int {
        var result: Int = propertyName.hashCode()
        result = 31 * result + value.hashCode()
        return result
    }

    override fun test(t: T): Boolean {
        val v: String = getValue(t) as? String ?: return false
        return v.contains(contains)
    }
}

/**
 * A LIKE filter which performs the case-insensitive 'substring' matching. Usually only used for in-memory
 * filtering since it performs quite poorly in the database.
 *
 * *WARNING:* The database performance is very poor, even on indexed columns - the database effectively performs full
 * table scan. Instead you should use the [FullTextFilter] and the full text search
 * capabilities of your database; alternatively use [NativeSqlFilter] to write a query suited
 * for the database of your choice. For example see [PostgreSQL full-text search](https://www.postgresql.org/docs/9.5/static/textsearch.html).
 * @param contains the substring, automatically prepended and appended with `%` when the SQL query is constructed. The 'substring' is matched
 * case-insensitive.
 */
class ISubstringFilter<T : Any>(override val propertyName: DataLoaderPropertyName, contains: String) : BeanFilter<T>() {
    /**
     * Any probe must contain this string.
     */
    val contains: String = contains.trim()
    override val value = "%${this.contains}%"
    override fun toString() = "$propertyName ILIKE $formattedValue"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as ISubstringFilter<*>
        if (propertyName != other.propertyName) return false
        if (value != other.value) return false
        return true
    }

    override fun hashCode(): Int {
        var result: Int = propertyName.hashCode()
        result = 31 * result + value.hashCode()
        return result
    }

    override fun test(t: T): Boolean {
        val v: String = getValue(t) as? String ?: return false
        return v.contains(contains, ignoreCase = true)
    }
}

/**
 * A FullText filter which performs the case-insensitive full-text search.
 * Any probe text must either contain all words in this query,
 * or the query words must match beginnings of all of the words contained in the probe string.
 *
 * Different implementors will implement this differently and may have different
 * requirements on how to configure the storage for full-text search:
 * * For example to have a full-text search in an SQL database/RDBMS system please use the
 *   [vok-orm](https://github.com/mvysny/vok-orm) library and read the
 *   [Full-Text Filters chapter](https://github.com/mvysny/vok-orm#full-text-filters).
 * @param query any probe text must either contain words in this query,
 * or the query words must match beginnings of the words contained in the probe.
 */
class FullTextFilter<T : Any>(override val propertyName: DataLoaderPropertyName, query: String) : BeanFilter<T>() {
    /**
     * In order for the probe to match, the probe must either match these words,
     * or the query words must match beginnings of the words contained in the probe.
     */
    val words: Set<String> = query.trim().toLowerCase().splitToWords().toSet()
    override val value = "~$words"
    override fun toString() = "$propertyName ~ $words"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as FullTextFilter<*>
        if (propertyName != other.propertyName) return false
        if (words != other.words) return false
        return true
    }

    override fun hashCode(): Int {
        var result: Int = propertyName.hashCode()
        result = 31 * result + value.hashCode()
        return result
    }

    override fun test(t: T): Boolean {
        val v: String = getValue(t) as? String ?: return false
        if (words.isEmpty()) {
            return true
        }
        val probe: Set<String> = v.trim().toLowerCase().splitToWords().toSet()
        for (word: String in words) {
            if (!probe.contains(word) && probe.none { it.startsWith(word) }) {
                return false
            }
        }
        return true
    }
}
