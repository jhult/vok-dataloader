package com.github.mvysny.vokdataloader

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import kotlin.test.expect

class DataLoaderTest : DynaTest({
    group("filter") {
        test("simple") {
            val dl: DataLoader<Person> = (0..10).map { Person("name $it", it) }.dataLoader().withFilter { Person::age ge 5 }
            expect((5..10).toList()) { dl.fetch().map { it.age } }
            expect(6) { dl.getCount() }
        }
        test("combining filters") {
            val dl: DataLoader<Person> = (0..10).map { Person("name $it", it) }.dataLoader().withFilter { Person::age ge 5 }
            expect((5..7).toList()) { dl.fetch(filter = buildFilter { Person::age le 7 }).map { it.age } }
            expect(3) { dl.getCount(filter = buildFilter { Person::age le 7 }) }
        }
    }
    group("sorted") {
        test("simple") {
            val dl: DataLoader<Person> = (0..10).map { Person("name $it", it) }.dataLoader().sortedBy(Person::age.desc)
            expect((10 downTo 0).toList()) { dl.fetch().map { it.age } }
            expect(11) { dl.getCount() }
        }

        test("overriding sort") {
            val dl: DataLoader<Person> = (0..9).map { Person("name $it", it) }.dataLoader().sortedBy(Person::age.desc)
            expect((0..9).toList()) { dl.fetch(sortBy = listOf(Person::name.asc)).map { it.age } }
        }

        test("combining sort") {
            val dl: DataLoader<Person> = (0..9).map { Person("name $it", it / 3) }.dataLoader().sortedBy(Person::age.desc)
            expectList(9, 6, 7, 8, 3, 4, 5, 0, 1, 2) { dl.fetch().map { it.name!!.removePrefix("name ").toInt() } }
            expectList(9, 8, 7, 6, 5, 4, 3, 2, 1, 0) { dl.fetch(sortBy = listOf(Person::name.desc)).map { it.name!!.removePrefix("name ").toInt() } }
        }
    }
})
