package com.github.mvysny.vokdataloader

import com.github.mvysny.dynatest.DynaTest
import kotlin.test.expect

/**
 * @author Martin Vysny <mavi@vaadin.com>
 */
class PropertyPathTest : DynaTest({
    group("simple path") {
        test("getValue") {
            var person = Person("foo", 4)
            expect("foo") { Person::name.propertyPath.getValue(person) }
            expect(4) { Person::age.propertyPath.getValue(person) }
            person = Person(null)
            expect(null) { Person::name.propertyPath.getValue(person) }
        }

        test("toString") {
            expect("PropertyPath(Person.age)") { Person::age.propertyPath.toString() }
            expect("PropertyPath(Person.name)") { Person::name.propertyPath.toString() }
        }

        test("valueType") {
            expect(Int::class.java) { Person::age.propertyPath.valueType }
            expect(String::class.java) { Person::name.propertyPath.valueType }
        }
    }

    group("complex path") {
        test("getValue() returns null if anything on the path evaluates to null") {
            val c = OneManCompany(null)
            val path = PropertyPath.of(OneManCompany::class.java, "employee.person.name")
            expect(null) { path(null) }
            expect(null) { path(c) }
            c.employee = Employee(null)
            expect(null) { path(c) }
            c.employee!!.person = Person(null)
            expect(null) { path(c) }
        }

        test("getValue() returns non-null value") {
            val c = OneManCompany(Employee(Person("Foo")))
            val path = PropertyPath.of(OneManCompany::class.java, "employee.person.name")
            expect("Foo") { path(c) }
        }

        test("toString") {
            expect("PropertyPath(OneManCompany.employee.person.name)") { PropertyPath.of(OneManCompany::class.java, "employee.person.name").toString() }
        }

        test("valueType") {
            val path = PropertyPath.of(OneManCompany::class.java, "employee.person.name")
            expect(String::class.java) { path.valueType }
        }
    }
})

data class Employee(var person: Person?)
data class OneManCompany(var employee: Employee?)
